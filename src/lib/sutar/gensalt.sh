#-------------------------------------------------------------------------------
# SPDX-License-Identifier: "Apache-2.0"
# Copyright (C) 2020, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
sutar_gensalt_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar gensalt" "Generate Salt"
}

sutar_gensalt() {
  if [[ $# -gt 0 ]]; then
    sutar_gensalt_print_help
    exit 1
  fi

  openssl rand -hex 8
  printf "\n"
}

