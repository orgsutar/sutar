#-------------------------------------------------------------------------------
# SPDX-License-Identifier: "Apache-2.0"
# Copyright (C) 2020, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
sutar_changelog_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar changelog <args>" "Generate Password"

  echo "Arguments:"
  printf "  %-40s %-30s\n"  "-b|--base-link <base-link>" "Base Link for Commits"
  printf "  %-40s %-30s\n"  "-s|--start <start>" "Start Refname"
  echo "Optional Arguments:"
  printf "  %-40s %-30s\n"  "-e|--end <end>" "End Refname"
}

sutar_changelog() {
  START_SPECIFIED="false"
  END_SPECIFIED="false"
  while [[ $# -gt 0 ]]; do
    KEY=$1
    case $KEY in
      -h|--help)
        sutar_changelog_print_help
        exit 0
        ;;
      -b|--base-link)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        BASELINK=$1
        BASELINK_SPECIFIED="true"
        shift
        ;;
      -s|--start)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        START=$1
        START_SPECIFIED="true"
        shift
        ;;
      -e|--end)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
      END=$1
      END_SPECIFIED="true"
      shift
      ;;
    *)
      echo "Unrecognized Option $KEY"
      sutar_changelog_print_help
      exit 1
  esac
done

PRINT_HELP_AND_EXIT="false"

if [[ $START_SPECIFIED == "false" ]]; then
  echo "Missing start."
  PRINT_HELP_AND_EXIT="true"
fi

if [[ $BASELINK_SPECIFIED == "false" ]]; then
  echo "Missing Baselink."
  PRINT_HELP_AND_EXIT="true"
fi


if [[ $END_SPECIFIED == "false" ]]; then
END="HEAD"
END_SPECIFIED="true"
fi


if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
  sutar_changelog_print_help
  exit 1
fi


git log $END...$START --pretty=format:"- [\[%h\]]($BASELINK/%H)  %s"  --reverse
}

