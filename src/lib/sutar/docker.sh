#-------------------------------------------------------------------------------
# SPDX-License-Identifier: "Apache-2.0"
# Copyright (C) 2020, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
sutar_docker_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar docker --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar docker <op> <args>" "Generate Password"

  echo ""
  echo "Operations:"
  printf "  %-40s %-30s\n"  "build" "Build"
  printf "  %-40s %-30s\n"  "login" "Login"
}

sutar_docker_build_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar docker build <args>" "Generate Password"

  echo ""
  echo "Arguments:"
  printf "  %-40s %-30s\n"  "-d|--directory" "Directory to Build In"
  printf "  %-40s %-30s\n"  "-f|--file" "File"
  printf "  %-40s %-30s\n"  "-n|--name" "Image Name"
  printf "  %-40s %-30s\n"  "-r|--registry" "Docker Registry"
  printf "  %-40s %-30s\n"  "-v|--version" "Version"
}

sutar_docker_build() {
  DIRECTORY_SPECIFIED="false"
  FILE_SPECIFIED="false"
  NAME_SPECIFIED="false"
  VERSION_SPECIFIED="false"
  REGISTRY_SPECIFIED="false"
  CLEAN_BUILD="false"
  while [[ $# -gt 0 ]]; do
    KEY=$1
    case $KEY in
      -h|--help)
        sutar_docker_build_print_help
        exit 0
        ;;
      -d|--directory)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        DIRECTORY=$1
        DIRECTORY_SPECIFIED="true"
        shift
        ;;
      -f|--file)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        FILE=$1
        FILE_SPECIFIED="true"
        shift
        ;;
      -r|--registry)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        REGISTRY=$1
        REGISTRY_SPECIFIED="true"
        shift
        ;;
      -n|--name)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        NAME=$1
        NAME_SPECIFIED="true"
        shift
        ;;
      -v|--version)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        VERSION=$1
        VERSION_SPECIFIED="true"
        shift
        ;;
      -c|--clean-build)
        CLEAN_BUILD="true"
        shift
        ;;
      *)
        echo "Unrecognized Option $KEY"
        sutar_docker_build_print_help
        exit 1
    esac
  done

  PRINT_HELP_AND_EXIT="false"

  if [[ $DIRECTORY_SPECIFIED == "false" ]]; then
    echo "Missing Target."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $FILE_SPECIFIED == "false" ]]; then
    echo "Missing Target."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $REGISTRY_SPECIFIED == "false" ]]; then
    echo "Missing Host."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $NAME_SPECIFIED == "false" ]]; then
    echo "Missing Name."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $VERSION_SPECIFIED == "false" ]]; then
    echo "Missing Version."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
    sutar_docker_build_print_help
    exit 1
  fi

  if [[ $CLEAN_BUILD == "true" ]]; then
    CACHE_OPTION="--no-cache"
  else
    CACHE_OPTION=""
  fi
  docker build \
    --pull --network=host \
    $CACHE_OPTION \
    -t $REGISTRY/$NAME:$VERSION \
    -f $FILE \
    $DIRECTORY

  docker tag $REGISTRY/$NAME:$VERSION $REGISTRY/$NAME:$VERSION
  docker push $REGISTRY/$NAME:$VERSION
}

sutar_docker_login_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar docker login <args>" "Generate Password"

  echo ""
  echo "Arguments:"
  printf "  %-40s %-30s\n"  "-r|--registry" "Registry"
  printf "  %-40s %-30s\n"  "-p|--password" "Password"
  printf "  %-40s %-30s\n"  "-u|--username" "Username"
}

sutar_docker_login() {
  USERNAME_SPECIFIED="false"
  REGISTRY_SPECIFIED="false"
  PASSWORD_SPECIFIED="false"
  while [[ $# -gt 0 ]]; do
    KEY=$1
    case $KEY in
      -h|--help)
        sutar_docker_login_print_help
        exit 0
        ;;
      -u|--username)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        USERNAME=$1
        USERNAME_SPECIFIED="true"
        shift
        ;;
      -r|--registry)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        REGISTRY=$1
        REGISTRY_SPECIFIED="true"
        shift
        ;;
      -p|--password)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        PASSWORD=$1
        PASSWORD_SPECIFIED="true"
        shift
        ;;
      *)
        echo "Unrecognized Option $KEY"
        sutar_docker_login_print_help
        exit 1
    esac
  done

  PRINT_HELP_AND_EXIT="false"

  if [[ $USERNAME_SPECIFIED == "false" ]]; then
    echo "Missing Target."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $REGISTRY_SPECIFIED == "false" ]]; then
    echo "Missing Host."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $PASSWORD_SPECIFIED == "false" ]]; then
    echo "Missing Password."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
    sutar_docker_login_print_help
    exit 1
  fi

  docker login  -u $USERNAME -p $PASSWORD $REGISTRY
}

sutar_docker() {
  if [[ $# -eq 0 ]]; then
    sutar_docker_print_help
    exit 1
  fi


  SUBCOMMAND=$1
  shift
  case $SUBCOMMAND in
    -h|--help)
      sutar_docker_print_help
      exit 0
      ;;
    build)
      sutar_docker_build $@
      exit 0
      ;;
    login)
      sutar_docker_login $@
      exit 0
      ;;
    *)
      echo "Unrecognized Operation $SUBCOMMAND"
      sutar_docker_print_help
      exit 1
  esac
}

