#-------------------------------------------------------------------------------
# SPDX-License-Identifier: "Apache-2.0"
# Copyright (C) 2020, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
sutar_repo_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar repo --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar repo <op> <args>" "Generate Password"

  echo ""
  echo "Operations:"
  printf "  %-40s %-30s\n"  "sync" "Sync"
}

sutar_repo_sync_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar repo sync <args>" "Generate Password"

  echo ""
  echo "Arguments:"
  printf "  %-40s %-30s\n"  "-s|--source" "Source"
  printf "  %-40s %-30s\n"  "-d|--destination" "Destination"
}

sutar_repo_sync() {
  SOURCE_SPECIFIED="false"
  REPOR_SPECIFIED="false"
  while [[ $# -gt 0 ]]; do
    KEY=$1
    case $KEY in
      -h|--help)
        sutar_repo_sync_print_help
        exit 0
        ;;
      -s|--source)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        SOURCE=$1
        SOURCE_SPECIFIED="true"
        shift
        ;;
      -d|--destination)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        DESTINATION=$1
        DESTINATION_SPECIFIED="true"
        shift
        ;;
      *)
        echo "Unrecognized Option $KEY"
        sutar_repo_sync_print_help
        exit 1
    esac
  done

  PRINT_HELP_AND_EXIT="false"

  if [[ $SOURCE_SPECIFIED == "false" ]]; then
    echo "Missing Target."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $DESTINATION_SPECIFIED == "false" ]]; then
    echo "Missing Host."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
    sutar_repo_sync_print_help
    exit 1
  fi

  TEMP=$(mktemp -d -t ci-XXXX)
  pushd $TEMP
    git clone --mirror $SOURCE src
    pushd src
      git remote add destination $DESTINATION
      git push --mirror destination
    popd
  popd

}

sutar_repo() {
  if [[ $# -eq 0 ]]; then
    sutar_repo_print_help
    exit 1
  fi


  SUBCOMMAND=$1
  shift
  case $SUBCOMMAND in
    -h|--help)
      sutar_repo_print_help
      exit 0
      ;;
    sync)
      sutar_repo_sync $@
      exit 0
      ;;
    *)
      echo "Unrecognized Operation $SUBCOMMAND"
      sutar_repo_print_help
      exit 1
  esac
}

