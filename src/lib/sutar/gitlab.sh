#-------------------------------------------------------------------------------
# SPDX-License-Identifier: "Apache-2.0"
# Copyright (C) 2020, Jayesh Badwaik <j.badwaik@fz-juelich.de>
#-------------------------------------------------------------------------------
sutar_gitlab_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar gitlab --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar gitlab <op> <args>" "Conduction Operation <op> with <args>"

  echo ""
  echo "Operations:"
  printf "  %-40s %-30s\n"  "register" "register"
}

sutar_gitlab_register_print_help()
{
  echo "Usage:"
  printf "  %-30s %-30s\n"  "sutar --help"  "Display this help"
  printf "  %-30s %-30s\n"  "sutar gitlab register <args>" "Generate Password"

  echo ""
  echo "Arguments:"
  printf "  %-40s %-30s\n"  "-u|--url" "URL"
  printf "  %-40s %-30s\n"  "-t|--token" "Token"
  printf "  %-40s %-30s\n"  "-n|--name" "Name"
  printf "  %-40s %-30s\n"  "-e|--executor" "Executor"
  printf "  %-40s %-30s\n"  "[--arch arch]" "Architecture"
}

sutar_gitlab_register() {
  URL_SPECIFIED="false"
  TOKEN_SPECIFIED="false"
  NAME_SPECIFIED="false"
  EXECUTOR_SPECIFIED="false"
  ARCH_SPECIFIED="false"
  while [[ $# -gt 0 ]]; do
    KEY=$1
    case $KEY in
      -h|--help)
        sutar_gitlab_register_print_help
        exit 0
        ;;
      -u|--url)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        URL=$1
        URL_SPECIFIED="true"
        shift
        ;;
      -t|--token)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        TOKEN=$1
        TOKEN_SPECIFIED="true"
        shift
        ;;
      -n|--name)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        NAME=$1
        NAME_SPECIFIED="true"
        shift
        ;;
      -e|--executor)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        EXECUTOR=$1
        EXECUTOR_SPECIFIED="true"
        shift
        ;;
      --arch)
        shift
        if [[ $# -eq 0 ]]; then
          break
        fi
        ARCH=$1
        ARCH_SPECIFIED="true"
        shift
        ;;
      *)
        echo "Unrecognized Option $KEY"
        sutar_gitlab_register_print_help
        exit 1
    esac
  done

  PRINT_HELP_AND_EXIT="false"

  if [[ $URL_SPECIFIED == "false" ]]; then
    echo "Missing Target."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $TOKEN_SPECIFIED == "false" ]]; then
    echo "Missing Target."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $NAME_SPECIFIED == "false" ]]; then
    echo "Missing Host."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $EXECUTOR_SPECIFIED == "false" ]]; then
    echo "Missing Name."
    PRINT_HELP_AND_EXIT="true"
  fi

  if [[ $ARCH_SPECIFIED == "false" ]]; then
    ARCH="x64"
  fi



  if [[ $PRINT_HELP_AND_EXIT == "true" ]]; then
    sutar_gitlab_register_print_help
    exit 1
  fi

  RUNNER_ARGS=()

  RUNNER_ARGS+=( --executor docker )
  RUNNER_ARGS+=( --docker-cap-add SYS_PTRACE )
  RUNNER_ARGS+=( --docker-image ruby:2.7 )
  RUNNER_ARGS+=( --docker-volumes '/cache' )
  RUNNER_ARGS+=( --non-interactive )
  RUNNER_ARGS+=( --docker-pull-policy always)
  RUNNER_ARGS+=( --docker-pull-policy if-not-present )
  RUNNER_ARGS+=( --docker-volumes '/var/run/docker.sock:/var/run/docker.sock' )
  RUNNER_ARGS+=( --docker-privileged )

  if [[ $EXECUTOR == "generic" ]]; then
    RUNNER_ARGS+=( --tag-list "docker,generic" )
  elif  [[ $EXECUTOR == "dind" ]]; then
    RUNNER_ARGS+=( --tag-list "docker,dind" )
  elif  [[ $EXECUTOR == "x64" ]]; then
    RUNNER_ARGS+=( --tag-list "docker,x64" )
  elif  [[ $EXECUTOR == "aarch64" ]]; then
    RUNNER_ARGS+=( --tag-list "docker,aarch64" )
  elif  [[ $EXECUTOR == "cuda" ]]; then
    RUNNER_ARGS+=( --tag-list "docker,cuda" )
    RUNNER_ARGS+=( --docker-gpus "all" )
  elif  [[ $EXECUTOR == "hip" ]]; then
    RUNNER_ARGS+=( --tag-list "docker,hip" )
  else
    echo "ERROR: Unrecognized Executor $EXECUTOR. Exiting..."
    exit 1
  fi

  gitlab-runner register \
    --url $URL --registration-token $TOKEN \
    --name $NAME "${RUNNER_ARGS[@]}"

  #STOP
}


sutar_gitlab() {
  if [[ $# -eq 0 ]]; then
    sutar_gitlab_print_help
    exit 1
  fi

  SUBCOMMAND=$1
  shift
  case $SUBCOMMAND in
    -h|--help)
      sutar_gitlab_print_help
      exit 0
      ;;
    register)
      sutar_gitlab_register $@
      exit 0
      ;;
    *)
      echo "Unrecognized Operation $SUBCOMMAND"
      sutar_gitlab_print_help
      exit 1
  esac
}

